function prompt
{
    Write-Host $(Get-Date -Format [HH:mm:ss])  -NoNewline -ForegroundColor Blue
	write-host $(get-location) -nonewline -foregroundcolor green
    return ">"
}


ipmo psreadline
#ipmo tabexpansion++

Import-Module PSReadLine
# More logical colors for Twilight?
Set-PSReadlineOption -TokenKind Comment -ForegroundColor DarkGray
Set-PSReadlineOption -TokenKind Keyword -ForegroundColor Yellow
Set-PSReadlineOption -TokenKind String -ForegroundColor Green
Set-PSReadlineOption -TokenKind Operator -ForegroundColor DarkGreen
Set-PSReadlineOption -TokenKind Variable -ForegroundColor cyan
Set-PSReadlineOption -TokenKind Command -ForegroundColor DarkYellow
Set-PSReadlineOption -TokenKind Parameter -ForegroundColor DarkCyan
Set-PSReadlineOption -TokenKind Type -ForegroundColor Blue
Set-PSReadlineOption -TokenKind Number -ForegroundColor Red
Set-PSReadlineOption -TokenKind Member -ForegroundColor DarkRed
Set-PSReadlineOption -ContinuationPromptForegroundColor DarkBlue -ContinuationPrompt (([char]183) + "  ")

Set-PSReadlineKeyHandler -Key Ctrl+R -Function ReverseSearchHistory
Set-PSReadlineKeyHandler -Key Ctrl+Shift+R -Function ForwardSearchHistory

Set-PSReadlineKeyHandler Ctrl+K KillLine
Set-PSReadlineKeyHandler Ctrl+I Yank

