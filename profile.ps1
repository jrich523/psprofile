
function Title{ if($psise){$psise.CurrentPowerShellTab.DisplayName = $args -join " "}else{$host.ui.RawUI.WindowTitle = $args -join " "}}

function Format-Definition{
 [CmdletBinding()]
 Param( [Parameter(Mandatory=$true, ValueFromPipeline=$true, Position=0)]
 [System.Management.Automation.PSMethod] $method)

    $defs = $method.OverloadDefinitions
    foreach($def in $defs)
    {
        if($def -like "*()")
            {Write-Host $def -f 3}
        else
        {
            $splits = $def -split '\(|\)|,'
            write-host $splits[0] -f 3
            $splits[1..100] | %{
                $rtn = $_.trim() -split " "
                Write-host "`t$($rtn[0]) " -F 12 -NoNewline
                write-host $rtn[1] -f 13
}}}}

function Select-AllProperty
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        $object
    )
    process{
     select -InputObject $object -Property *
     }
}
Set-Alias -Name sap -Value Select-AllProperty


###############Set Aliasns
Set-Alias -Name tc -Value Test-Connection
set-alias -Name fd -Value Format-Definition

############### V3 default params
$PSDefaultParameterValues.'test-connection:count' = 1
$PSDefaultParameterValues.'export-csv:notypeinformation' = $true
$PSDefaultParameterValues.'Get-Childitem:force' = $true
$PSDefaultParameterValues.'Get-Help:ShowWindow' = $true
$PSDefaultParameterValues.'Format-Table:Autosize' = $true


### function for finding variables i've created in my session, must be at end of profile to ignore profile var's
$DefaultVariableList = ""
$DefaultVariableList = gv | select -exp name
function Get-MyVariables
{
    gv | ? {$DefaultVariableList -notcontains $_.name}
}
set-alias -Name gmv -Value Get-MyVariables
#####

if(Test-Path d:\ps)
{cd d:\ps}