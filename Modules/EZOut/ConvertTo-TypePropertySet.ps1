function ConvertTo-TypePropertySet
{
    param(
    [ValidateScript({
        if ($_.pstypenames[0] -notlike "Selected.*") {
            throw "Must pipe in the result of a select-object"
        }
        return $true
    })]
    [Parameter(ValueFromPipeline=$true,Mandatory=$true)]
    $InputObject,
    
    [Parameter(Mandatory=$true,Position=0)]
    $Name
    )
    
    begin {
        $list = New-Object Collections.ArrayList
    }
    
    process {
        $null = $list.Add($inputObject)
    }
    end { 
        $groupedByType = $list | Group-Object { $_.pstypenames[0] } -AsHashTable

        foreach ($kv in $groupedByType.GetEnumerator()) {
            $shortTypeName = $kv.Key.Substring("Selected.".Length)
            $values = $kv.Value | Get-Member -MemberType Properties | Select-Object -ExpandProperty Name            
            Write-TypeView -TypeName $shortTypeName -PropertySet @{
                $Name = $values
            }
        }
            
        
        

    }    
    
    
}