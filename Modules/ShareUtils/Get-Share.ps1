﻿function Get-Share {
<#
.Synopsis
    Retrieves specified network share from a local or remote computer.
.Description
    Retrieves specified network share (or shares) from a local or remote computer (or computers). Only user shares
	are supported by this cmdlet. Special shares such IPC$, ADMIN$, etc. are not supported.
.Parameter Name
	Specifies network share name or names to retrieve. If parameter is omitted, the cmdlet will retrieve
	all available network shares from specified computer (or computers). The parameter accepts astersisk '*'
	as a wildcard sign.
.Parameter ComputerName
    Specifies remote computer or computers to process. By default local computer is used.
.EXAMPLE
	PS > Get-Share Backups

	This will retrieve only single share named Backups from local computer
.EXAMPLE
	PS > Get-Share Installs, Docs -ComputerName FileServ1, FileServ2

	Retrieves Installs and Docs shares from FileServ1 and FileServ2 computers (if possible)
.EXAMPLE
	PS > Get-Share U*
	
	Retrieves all shares which names starts with "U" character.
.Outputs
	ShareUtils.ShareInfo
.NOTES
	Author: Vadims Podans
	Blog  : http://en-us.sysadmins.lv
.LINK
	Add-SharePermission
	Set-SharePermission
	Remove-SharePermission
	Remove-Share
#>
[OutputType('ShareUtils.ShareInfo')]
[CmdletBinding()]
	param(
		[String[]]$Name,
		[String[]]$ComputerName = $env:COMPUTERNAME
	)
	$shares = @()
	foreach ($computer in $ComputerName) {
		Write-Verbose "Process computer: $computer"
		if ($Name -ne $null) {
			foreach ($sharename in $Name) {
				Write-Verbose "Parameter [Name] '$sharename' is present, retrieve specified share on specified computer."
				$share1 = gwmi Win32_Share -ComputerName $Computer -Filter "name LIKE '$($sharename.Replace("*","%"))'"
				if ($share1 -ne $null -and $share1.Type -eq 0) {
					$shares += $share1
				} else {Write-Verbose "share '$sharename' is not found on '$computer'"}
				
			}
			Write-Verbose "Collected $(@($shares).count) shares for computer '$computer'"
		} else {
			Write-Verbose "Parameter [Name] is not present, retrieve any available share on the computer '$computer'."
			$share1 += gwmi Win32_Share -ComputerName $Computer -Filter "type = 0"
			if ($share1 -ne $null) {$shares += $share1} else {Write-Verbose "no share is found on '$computer'"}
			Write-Verbose "Collected $(@($shares).count) shares for computer $computer"
		}
	}
	foreach ($share in $shares) {
		if ($share) {
			Write-Verbose "Create custom ShareUtils.ShareInfo object for share: '$($share.Name)'"
			$ShareInfo = New-Object ShareUtils.ShareInfo
			$ShareInfo.ComputerName = $share.__SERVER
			$ShareInfo.Name = $share.Name
			$ShareInfo.Path = $share.Path
			$ShareInfo.Description = $share.Description
			$ShareInfo.AllowMaximum = $share.AllowMaximum
			$ShareInfo.MaximumAllowed = [int]$share.MaximumAllowed
			Write-Verbose "Try to retrieve security information about current share"
			$ShareSec = gwmi Win32_LogicalShareSecuritySetting -ComputerName $ShareInfo.ComputerName -filter "name='$($share.name)'"
			if ($shareSec) {
				Write-Verbose "Try to extract security descriptor object"
				$SD = $sharesec.GetSecurityDescriptor()
				$SD.Descriptor.DACL | %{
					Write-Verbose "Create SecurityDescriptor object for current ACE"
					$Descriptor = New-Object ShareUtils.SecurityDescriptor.SD
					$Descriptor.User = $_.trustee.Name
					$Descriptor.SIDString = $_.trustee.SIDString
					$Descriptor.Domain = $_.trustee.Domain
					$Descriptor.AccessMask = $_.AccessMask
					$Descriptor.AceFlags = $_.AceFlags
					$Descriptor.AceType = $_.AceType
					$ShareInfo.SecurityDescriptor += $Descriptor
				}
			} else {
				Write-Error "Specified share '$($share.__SERVER)' does not exist or you may not have sufficient rights to access them!"
			}
			Write-Verbose "Return new ShareInfo object"
			$ShareInfo
		} else {Write-Verbose "No security information could be retrieved from share name: '$($share.name)'"}
	}
}
# SIG # Begin signature block
# MIIVAwYJKoZIhvcNAQcCoIIU9DCCFPACAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU7rt3iJP2Oefrmjyl3vWK2sZ9
# VmegghDIMIIDejCCAmKgAwIBAgIQOCXX+vhhr570kOcmtdZa1TANBgkqhkiG9w0B
# AQUFADBTMQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xKzAp
# BgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EwHhcNMDcw
# NjE1MDAwMDAwWhcNMTIwNjE0MjM1OTU5WjBcMQswCQYDVQQGEwJVUzEXMBUGA1UE
# ChMOVmVyaVNpZ24sIEluYy4xNDAyBgNVBAMTK1ZlcmlTaWduIFRpbWUgU3RhbXBp
# bmcgU2VydmljZXMgU2lnbmVyIC0gRzIwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJ
# AoGBAMS18lIVvIiGYCkWSlsvS5Frh5HzNVRYNerRNl5iTVJRNHHCe2YdicjdKsRq
# CvY32Zh0kfaSrrC1dpbxqUpjRUcuawuSTksrjO5YSovUB+QaLPiCqljZzULzLcB1
# 3o2rx44dmmxMCJUe3tvvZ+FywknCnmA84eK+FqNjeGkUe60tAgMBAAGjgcQwgcEw
# NAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC52ZXJpc2ln
# bi5jb20wDAYDVR0TAQH/BAIwADAzBgNVHR8ELDAqMCigJqAkhiJodHRwOi8vY3Js
# LnZlcmlzaWduLmNvbS90c3MtY2EuY3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMI
# MA4GA1UdDwEB/wQEAwIGwDAeBgNVHREEFzAVpBMwETEPMA0GA1UEAxMGVFNBMS0y
# MA0GCSqGSIb3DQEBBQUAA4IBAQBQxUvIJIDf5A0kwt4asaECoaaCLQyDFYE3CoIO
# LLBaF2G12AX+iNvxkZGzVhpApuuSvjg5sHU2dDqYT+Q3upmJypVCHbC5x6CNV+D6
# 1WQEQjVOAdEzohfITaonx/LhhkwCOE2DeMb8U+Dr4AaH3aSWnl4MmOKlvr+ChcNg
# 4d+tKNjHpUtk2scbW72sOQjVOCKhM4sviprrvAchP0RBCQe1ZRwkvEjTRIDroc/J
# ArQUz1THFqOAXPl5Pl1yfYgXnixDospTzn099io6uE+UAKVtCoNd+V5T9BizVw9w
# w/v1rZWgDhfexBaAYMkPK26GBPHr9Hgn0QXF7jRbXrlJMvIzMIIDxDCCAy2gAwIB
# AgIQR78Zld+NUkZD99ttSA0xpDANBgkqhkiG9w0BAQUFADCBizELMAkGA1UEBhMC
# WkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUx
# DzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3RlIENlcnRpZmljYXRpb24x
# HzAdBgNVBAMTFlRoYXd0ZSBUaW1lc3RhbXBpbmcgQ0EwHhcNMDMxMjA0MDAwMDAw
# WhcNMTMxMjAzMjM1OTU5WjBTMQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNp
# Z24sIEluYy4xKzApBgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2Vydmlj
# ZXMgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpyrKkzM0grwp9
# iayHdfC0TvHfwQ+/Z2G9o2Qc2rv5yjOrhDCJWH6M22vdNp4Pv9HsePJ3pn5vPL+T
# rw26aPRslMq9Ui2rSD31ttVdXxsCn/ovax6k96OaphrIAuF/TFLjDmDsQBx+uQ3e
# P8e034e9X3pqMS4DmYETqEcgzjFzDVctzXg0M5USmRK53mgvqubjwoqMKsOLIYdm
# vYNYV291vzyqJoddyhAVPJ+E6lTBCm7E/sVK3bkHEZcifNs+J9EeeOyfMcnx5iIZ
# 28SzR0OaGl+gHpDkXvXufPF9q2IBj/VNC97QIlaolc2uiHau7roN8+RN2aD7aKCu
# FDuzh8G7AgMBAAGjgdswgdgwNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhho
# dHRwOi8vb2NzcC52ZXJpc2lnbi5jb20wEgYDVR0TAQH/BAgwBgEB/wIBADBBBgNV
# HR8EOjA4MDagNKAyhjBodHRwOi8vY3JsLnZlcmlzaWduLmNvbS9UaGF3dGVUaW1l
# c3RhbXBpbmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYDVR0PAQH/BAQD
# AgEGMCQGA1UdEQQdMBukGTAXMRUwEwYDVQQDEwxUU0EyMDQ4LTEtNTMwDQYJKoZI
# hvcNAQEFBQADgYEASmv56ljCRBwxiXmZK5a/gqwB1hxMzbCKWG7fCCmjXsjKkxPn
# BFIN70cnLwA4sOTJk06a1CJiFfc/NyFPcDGA8Ys4h7Po6JcA/s9Vlk4k0qknTnqu
# t2FB8yrO58nZXt27K4U+tZ212eFX/760xX71zwye8Jf+K9M7UhsbOCf3P0owggSn
# MIIDj6ADAgECAgphnWDwAAAAAAACMA0GCSqGSIb3DQEBBQUAMH4xCzAJBgNVBAYT
# AkxWMRUwEwYDVQQKEwxTeXNhZG1pbnMgTFYxHDAaBgNVBAsTE0luZm9ybWF0aW9u
# IFN5c3RlbXMxOjA4BgNVBAMTMVN5c2FkbWlucyBMViBDbGFzcyAxIFJvb3QgQ2Vy
# dGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTAwNDE0MTc0MTE2WhcNMjAwNDE0MTYy
# NTU1WjByMQswCQYDVQQGEwJMVjEVMBMGA1UEChMMU3lzYWRtaW5zIExWMRwwGgYD
# VQQLExNJbmZvcm1hdGlvbiBTeXN0ZW1zMS4wLAYDVQQDEyVTeXNhZG1pbnMgTFYg
# SW50ZXJuYWwgQ2xhc3MgMSBTdWJDQS0xMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
# MIIBCgKCAQEAuHHw5SbkYZip0ZeLh2vKLXT6U5FHwKZDWGhqD5fFRKvMdwbhcDOj
# WDkMFLAGAOaut0nRsdtWn59vghcZxbHQGNaB1otcnL9cVgliGKaKiP/i3GbXwpOC
# RIOeVoldKpSOR1qlN8AWTXUXpjRBUp5Dgymi0Cnj7kKpn1w45Iea49oIHGUM8v64
# NHrpY6rv9EQDyE98/qjMMpHZkJlOAeGm+mL1bgyGWGg0kXyBYOZ/e7xCOia70u0+
# t5aUdWgAx2SSIuUholnyBStGMPcPrJtUVHk9Ygdc/W8Dg7bZQPFGDioPvYNI35v6
# fceKi7cSgtwj8xqRqG7cynfqx2lnqSLFjQIDAQABo4IBMTCCAS0wEAYJKwYBBAGC
# NxUBBAMCAQAwHQYDVR0OBBYEFBv6XnMtZxNcztMO5uh6qWCMC2P8MBkGCSsGAQQB
# gjcUAgQMHgoAUwB1AGIAQwBBMAsGA1UdDwQEAwIBhjAPBgNVHRMBAf8EBTADAQH/
# MB8GA1UdIwQYMBaAFE1IjcunX0Cos22iqY5OcfxPFhnUMDYGA1UdHwQvMC0wK6Ap
# oCeGJWh0dHA6Ly93d3cuc3lzYWRtaW5zLmx2L3BraS9yY2EtMS5jcmwwaAYIKwYB
# BQUHAQEEXDBaMDEGCCsGAQUFBzAChiVodHRwOi8vd3d3LnN5c2FkbWlucy5sdi9w
# a2kvcmNhLTEuY3J0MCUGCCsGAQUFBzABhhlodHRwOi8vb2NzcC5zeXNhZG1pbnMu
# bHYvMA0GCSqGSIb3DQEBBQUAA4IBAQCscUpz7yWwjkKRDmaolN9o0HjU6FvRfXAz
# EkL9JuymyDN/fvFfCHqqM49GNGAlh2ESemHisfS2Gf/dS0B8uSYSxiaNH8RSOOK1
# Tr8xvr+W/2vsVBiYFA0/SciJStBjBrcOKhwy2zW/dQMOEX86qPyEKqGAR1gsyNsO
# yABSBFCRsK3Tw+tlbRXldyj2pYBt1XxHuzPiZMA1Zz8O4rwcJRNLD6KNi49K49c7
# S1/9GEyT31TRTAx08VgLzLZ6kCSToGHM/mLeNUpW2ondzje6nqdBmxRHg++wrAKX
# 05DRuRri8MAVtaBwHxgQb+RO6KqZNoSVHZJ/0b7SSaZQgQW66zXXMIIE0zCCA7ug
# AwIBAgIKYTydVQAAAAAAEzANBgkqhkiG9w0BAQUFADByMQswCQYDVQQGEwJMVjEV
# MBMGA1UEChMMU3lzYWRtaW5zIExWMRwwGgYDVQQLExNJbmZvcm1hdGlvbiBTeXN0
# ZW1zMS4wLAYDVQQDEyVTeXNhZG1pbnMgTFYgSW50ZXJuYWwgQ2xhc3MgMSBTdWJD
# QS0xMB4XDTEwMDQxNTE3NDA1NloXDTE1MDQxNDE3NDA1NlowWjELMAkGA1UEBxMC
# TFYxFTATBgNVBAoTDFN5c2FkbWlucyBMVjEcMBoGA1UECxMTSW5mb3JtYXRpb24g
# U3lzdGVtczEWMBQGA1UEAxMNVmFkaW1zIFBvZGFuczCCASIwDQYJKoZIhvcNAQEB
# BQADggEPADCCAQoCggEBAIcw8V5Bjn11ZLAG/GhiQ+y7CEpYt/Z6alFQdkBNPSHu
# WMC+ebPUQgEky57JOeo9DeXUv8+rOxOt1thptEDEIZ5tJQHhSxLEfoxLSHQCkn4O
# mQXk6q/UZWfvktv73k2Rq+xdtvmMFTH4xqvhddVma6MeKEBWPu5URhT7wvnI+cGh
# 5TeE8kmErq/E2hVIOeZ1r85IC1naBiV4VxJMMQkePswBTYCAcjYCT1UU8GihEdgq
# 8dClNmsE2a/dYNoTktxIGUk2wFnP/ptSEtrlzhczKa5WDlGeuMx62lfRuTfzq+gO
# zk4JDleud6NPqqIijh/iYBS+qJ+4GexYPL0wZCdTPVUCAwEAAaOCAYEwggF9MDsG
# CSsGAQQBgjcVBwQuMCwGJCsGAQQBgjcVCJadTYWSsni9nzyF6Ox0gs7YRHqCqvdC
# h+fENgIBZAIBAzAfBgNVHSUEGDAWBgorBgEEAYI3CgMMBggrBgEFBQcDAzAOBgNV
# HQ8BAf8EBAMCB4AwKQYJKwYBBAGCNxUKBBwwGjAMBgorBgEEAYI3CgMMMAoGCCsG
# AQUFBwMDMB0GA1UdDgQWBBQsddpa07a5NYAClLLmLzGiK9dXmTAfBgNVHSMEGDAW
# gBQb+l5zLWcTXM7TDuboeqlgjAtj/DA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8v
# d3d3LnN5c2FkbWlucy5sdi9wa2kvcGljYS0xLmNybDBpBggrBgEFBQcBAQRdMFsw
# MgYIKwYBBQUHMAKGJmh0dHA6Ly93d3cuc3lzYWRtaW5zLmx2L3BraS9waWNhLTEu
# Y3J0MCUGCCsGAQUFBzABhhlodHRwOi8vb2NzcC5zeXNhZG1pbnMubHYvMA0GCSqG
# SIb3DQEBBQUAA4IBAQBJ2bGbZu+3T+0ZJXOTSjQfXfAcBzzqHM+R16Up6qXkjUnQ
# gguINT/1Ktqr3y7SdPGkyHZytqz0ABwr/hgZ1bdl4WaV9xpy4oJni7wU4Gq6Mh8Q
# zvhGwrQmQifbRyumM/EKMzyYZU+KkD7TAHoN1CiEGhiEyK+9OVaQNxAxwO3jmWWN
# cj2Q86YrV7r+XzkAU/N6gSeVUXii5eGA30wQNnCWQd2cTzL9tHdNH8t4qKN9Lhij
# t0EoxGEZYGDniROmIYlIwZUj6nU/XsmeHyJ5vpcvBxu12AVQMNIUY+HzCLStKnCy
# Sd1htmJBemlaam0OPeYp7QSUKgwzm1+gK813GUzKMYIDpTCCA6ECAQEwgYAwcjEL
# MAkGA1UEBhMCTFYxFTATBgNVBAoTDFN5c2FkbWlucyBMVjEcMBoGA1UECxMTSW5m
# b3JtYXRpb24gU3lzdGVtczEuMCwGA1UEAxMlU3lzYWRtaW5zIExWIEludGVybmFs
# IENsYXNzIDEgU3ViQ0EtMQIKYTydVQAAAAAAEzAJBgUrDgMCGgUAoHgwGAYKKwYB
# BAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAc
# BgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUhX40
# zvJpd2wn0k302pbbkN4fdg0wDQYJKoZIhvcNAQEBBQAEggEAIuRhDiwvrny8vZym
# nIyNilMUfXAWwDSA6zGatfsAnC0sY0cRDdqNA+YsJf7utgfq7JEz79QbJUUXsiC2
# O3T6zyZW9bANGXf8cbkdvU2d9x5wQp4sI8rEecuoM9C9e0BcBE2nvk+PtnF/7zse
# 3wesJ4ujHZQyCB1y1OQqPhROdrBrAQqiN1qDpWwcavFaGYz7/vjmdD6AvD/hS/R7
# uYGhpvd6fNvFOlwZlMpWZSeA7mCd5qjHy+ACz7HtULrRRznKtlF6rS5shV3MUANV
# 6QXTabDZx9mcIjAKyi2cxDcFZ03rE6rubgUPpmoZZ/JD0WTBc0xSUCu5l9bGgULx
# 2GCkJ6GCAX8wggF7BgkqhkiG9w0BCQYxggFsMIIBaAIBATBnMFMxCzAJBgNVBAYT
# AlVTMRcwFQYDVQQKEw5WZXJpU2lnbiwgSW5jLjErMCkGA1UEAxMiVmVyaVNpZ24g
# VGltZSBTdGFtcGluZyBTZXJ2aWNlcyBDQQIQOCXX+vhhr570kOcmtdZa1TAJBgUr
# DgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUx
# DxcNMTEwODExMTEyMTE2WjAjBgkqhkiG9w0BCQQxFgQUpcANO4M3FqFcwvFsMlD4
# ZHs8jPkwDQYJKoZIhvcNAQEBBQAEgYBeqrj9d+rlhgfDdnTC+s3bkNC/3DMoAMYU
# T8GHeiOLizgBw42BeFHWnS6UlL4WtYJoiDNrDHZBtZ7hxslkJP3Qf7nxvBPcApE/
# HReixkfGlvGcihiUP9mTuTMmZYKdYZmFmgrBwG/33Q8hV8zJeBa/9qJCojgO7FtN
# CFbhzEz/pw==
# SIG # End signature block
