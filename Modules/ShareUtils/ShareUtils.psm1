﻿function Test-ElevatedShell {
	$user = [Security.Principal.WindowsIdentity]::GetCurrent()
	(New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}
$warning = @"
	To run commands exposed by this module on Windows Vista, Windows Server 2008, and later versions of Windows,
	you must start an elevated Windows PowerShell console. You must have Administrator privligies on the remote
	computers and the remote registry service has to be running.
"@
# custom type definitions
Add-Type @'
namespace ShareUtils
{
	namespace SecurityDescriptor
	{
		public enum AccessType : int 
		{
			Allow,
			Deny
		}
		public enum Right : int
		{
			Read,
			Change,
			FullControl
		}
		public struct SD
		{
			public string User;
			public string SIDString;
			public string Domain;
			public AccessType AccessType;
			public Right Permission;
			public int AccessMask;
			public int AceType;
			public int AceFlags;
		}
	}
	public class ShareInfo
	{
		public string ComputerName;
		public string Name;
		public string Path;
		public string Description;
		public bool AllowMaximum;
		public int MaximumAllowed;
		public SecurityDescriptor.SD[] SecurityDescriptor;
	}
}
'@
function Get-ReturnCode ($Return) {
	switch ($Return.ReturnValue) {
        "0" {"Success"}
        "2" {"Access Denied"}
        "8" {"Unknown Failure"}
        "9" {"Invalid Name"}
        "21" {"Invalid Parameter"}
        "22" {"Duplicate Share"}
        "23" {"Redirected Path"}
        "24" {"Unknown Device or Directory"}
        "25" {"Net Name Not Found"}
        default {"Unknown error $write.ReturnValue"}
    }

}

# dot-source all function files
Get-ChildItem -Path $PSScriptRoot\*.ps1 | Foreach-Object{ . $_.FullName }
$RestrictedFunctions = "New-SD", "Get-ReturnCode"
Export-ModuleMember –Function @(Get-Command –Module $ExecutionContext.SessionState.Module | Where-Object {$RestrictedFunctions -notcontains $_.name})
# SIG # Begin signature block
# MIIVAwYJKoZIhvcNAQcCoIIU9DCCFPACAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUo/mJSKIoFlmGvGLCaDpQpU6G
# 4fOgghDIMIIDejCCAmKgAwIBAgIQOCXX+vhhr570kOcmtdZa1TANBgkqhkiG9w0B
# AQUFADBTMQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNpZ24sIEluYy4xKzAp
# BgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EwHhcNMDcw
# NjE1MDAwMDAwWhcNMTIwNjE0MjM1OTU5WjBcMQswCQYDVQQGEwJVUzEXMBUGA1UE
# ChMOVmVyaVNpZ24sIEluYy4xNDAyBgNVBAMTK1ZlcmlTaWduIFRpbWUgU3RhbXBp
# bmcgU2VydmljZXMgU2lnbmVyIC0gRzIwgZ8wDQYJKoZIhvcNAQEBBQADgY0AMIGJ
# AoGBAMS18lIVvIiGYCkWSlsvS5Frh5HzNVRYNerRNl5iTVJRNHHCe2YdicjdKsRq
# CvY32Zh0kfaSrrC1dpbxqUpjRUcuawuSTksrjO5YSovUB+QaLPiCqljZzULzLcB1
# 3o2rx44dmmxMCJUe3tvvZ+FywknCnmA84eK+FqNjeGkUe60tAgMBAAGjgcQwgcEw
# NAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC52ZXJpc2ln
# bi5jb20wDAYDVR0TAQH/BAIwADAzBgNVHR8ELDAqMCigJqAkhiJodHRwOi8vY3Js
# LnZlcmlzaWduLmNvbS90c3MtY2EuY3JsMBYGA1UdJQEB/wQMMAoGCCsGAQUFBwMI
# MA4GA1UdDwEB/wQEAwIGwDAeBgNVHREEFzAVpBMwETEPMA0GA1UEAxMGVFNBMS0y
# MA0GCSqGSIb3DQEBBQUAA4IBAQBQxUvIJIDf5A0kwt4asaECoaaCLQyDFYE3CoIO
# LLBaF2G12AX+iNvxkZGzVhpApuuSvjg5sHU2dDqYT+Q3upmJypVCHbC5x6CNV+D6
# 1WQEQjVOAdEzohfITaonx/LhhkwCOE2DeMb8U+Dr4AaH3aSWnl4MmOKlvr+ChcNg
# 4d+tKNjHpUtk2scbW72sOQjVOCKhM4sviprrvAchP0RBCQe1ZRwkvEjTRIDroc/J
# ArQUz1THFqOAXPl5Pl1yfYgXnixDospTzn099io6uE+UAKVtCoNd+V5T9BizVw9w
# w/v1rZWgDhfexBaAYMkPK26GBPHr9Hgn0QXF7jRbXrlJMvIzMIIDxDCCAy2gAwIB
# AgIQR78Zld+NUkZD99ttSA0xpDANBgkqhkiG9w0BAQUFADCBizELMAkGA1UEBhMC
# WkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTEUMBIGA1UEBxMLRHVyYmFudmlsbGUx
# DzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhhd3RlIENlcnRpZmljYXRpb24x
# HzAdBgNVBAMTFlRoYXd0ZSBUaW1lc3RhbXBpbmcgQ0EwHhcNMDMxMjA0MDAwMDAw
# WhcNMTMxMjAzMjM1OTU5WjBTMQswCQYDVQQGEwJVUzEXMBUGA1UEChMOVmVyaVNp
# Z24sIEluYy4xKzApBgNVBAMTIlZlcmlTaWduIFRpbWUgU3RhbXBpbmcgU2Vydmlj
# ZXMgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCpyrKkzM0grwp9
# iayHdfC0TvHfwQ+/Z2G9o2Qc2rv5yjOrhDCJWH6M22vdNp4Pv9HsePJ3pn5vPL+T
# rw26aPRslMq9Ui2rSD31ttVdXxsCn/ovax6k96OaphrIAuF/TFLjDmDsQBx+uQ3e
# P8e034e9X3pqMS4DmYETqEcgzjFzDVctzXg0M5USmRK53mgvqubjwoqMKsOLIYdm
# vYNYV291vzyqJoddyhAVPJ+E6lTBCm7E/sVK3bkHEZcifNs+J9EeeOyfMcnx5iIZ
# 28SzR0OaGl+gHpDkXvXufPF9q2IBj/VNC97QIlaolc2uiHau7roN8+RN2aD7aKCu
# FDuzh8G7AgMBAAGjgdswgdgwNAYIKwYBBQUHAQEEKDAmMCQGCCsGAQUFBzABhhho
# dHRwOi8vb2NzcC52ZXJpc2lnbi5jb20wEgYDVR0TAQH/BAgwBgEB/wIBADBBBgNV
# HR8EOjA4MDagNKAyhjBodHRwOi8vY3JsLnZlcmlzaWduLmNvbS9UaGF3dGVUaW1l
# c3RhbXBpbmdDQS5jcmwwEwYDVR0lBAwwCgYIKwYBBQUHAwgwDgYDVR0PAQH/BAQD
# AgEGMCQGA1UdEQQdMBukGTAXMRUwEwYDVQQDEwxUU0EyMDQ4LTEtNTMwDQYJKoZI
# hvcNAQEFBQADgYEASmv56ljCRBwxiXmZK5a/gqwB1hxMzbCKWG7fCCmjXsjKkxPn
# BFIN70cnLwA4sOTJk06a1CJiFfc/NyFPcDGA8Ys4h7Po6JcA/s9Vlk4k0qknTnqu
# t2FB8yrO58nZXt27K4U+tZ212eFX/760xX71zwye8Jf+K9M7UhsbOCf3P0owggSn
# MIIDj6ADAgECAgphnWDwAAAAAAACMA0GCSqGSIb3DQEBBQUAMH4xCzAJBgNVBAYT
# AkxWMRUwEwYDVQQKEwxTeXNhZG1pbnMgTFYxHDAaBgNVBAsTE0luZm9ybWF0aW9u
# IFN5c3RlbXMxOjA4BgNVBAMTMVN5c2FkbWlucyBMViBDbGFzcyAxIFJvb3QgQ2Vy
# dGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTAwNDE0MTc0MTE2WhcNMjAwNDE0MTYy
# NTU1WjByMQswCQYDVQQGEwJMVjEVMBMGA1UEChMMU3lzYWRtaW5zIExWMRwwGgYD
# VQQLExNJbmZvcm1hdGlvbiBTeXN0ZW1zMS4wLAYDVQQDEyVTeXNhZG1pbnMgTFYg
# SW50ZXJuYWwgQ2xhc3MgMSBTdWJDQS0xMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8A
# MIIBCgKCAQEAuHHw5SbkYZip0ZeLh2vKLXT6U5FHwKZDWGhqD5fFRKvMdwbhcDOj
# WDkMFLAGAOaut0nRsdtWn59vghcZxbHQGNaB1otcnL9cVgliGKaKiP/i3GbXwpOC
# RIOeVoldKpSOR1qlN8AWTXUXpjRBUp5Dgymi0Cnj7kKpn1w45Iea49oIHGUM8v64
# NHrpY6rv9EQDyE98/qjMMpHZkJlOAeGm+mL1bgyGWGg0kXyBYOZ/e7xCOia70u0+
# t5aUdWgAx2SSIuUholnyBStGMPcPrJtUVHk9Ygdc/W8Dg7bZQPFGDioPvYNI35v6
# fceKi7cSgtwj8xqRqG7cynfqx2lnqSLFjQIDAQABo4IBMTCCAS0wEAYJKwYBBAGC
# NxUBBAMCAQAwHQYDVR0OBBYEFBv6XnMtZxNcztMO5uh6qWCMC2P8MBkGCSsGAQQB
# gjcUAgQMHgoAUwB1AGIAQwBBMAsGA1UdDwQEAwIBhjAPBgNVHRMBAf8EBTADAQH/
# MB8GA1UdIwQYMBaAFE1IjcunX0Cos22iqY5OcfxPFhnUMDYGA1UdHwQvMC0wK6Ap
# oCeGJWh0dHA6Ly93d3cuc3lzYWRtaW5zLmx2L3BraS9yY2EtMS5jcmwwaAYIKwYB
# BQUHAQEEXDBaMDEGCCsGAQUFBzAChiVodHRwOi8vd3d3LnN5c2FkbWlucy5sdi9w
# a2kvcmNhLTEuY3J0MCUGCCsGAQUFBzABhhlodHRwOi8vb2NzcC5zeXNhZG1pbnMu
# bHYvMA0GCSqGSIb3DQEBBQUAA4IBAQCscUpz7yWwjkKRDmaolN9o0HjU6FvRfXAz
# EkL9JuymyDN/fvFfCHqqM49GNGAlh2ESemHisfS2Gf/dS0B8uSYSxiaNH8RSOOK1
# Tr8xvr+W/2vsVBiYFA0/SciJStBjBrcOKhwy2zW/dQMOEX86qPyEKqGAR1gsyNsO
# yABSBFCRsK3Tw+tlbRXldyj2pYBt1XxHuzPiZMA1Zz8O4rwcJRNLD6KNi49K49c7
# S1/9GEyT31TRTAx08VgLzLZ6kCSToGHM/mLeNUpW2ondzje6nqdBmxRHg++wrAKX
# 05DRuRri8MAVtaBwHxgQb+RO6KqZNoSVHZJ/0b7SSaZQgQW66zXXMIIE0zCCA7ug
# AwIBAgIKYTydVQAAAAAAEzANBgkqhkiG9w0BAQUFADByMQswCQYDVQQGEwJMVjEV
# MBMGA1UEChMMU3lzYWRtaW5zIExWMRwwGgYDVQQLExNJbmZvcm1hdGlvbiBTeXN0
# ZW1zMS4wLAYDVQQDEyVTeXNhZG1pbnMgTFYgSW50ZXJuYWwgQ2xhc3MgMSBTdWJD
# QS0xMB4XDTEwMDQxNTE3NDA1NloXDTE1MDQxNDE3NDA1NlowWjELMAkGA1UEBxMC
# TFYxFTATBgNVBAoTDFN5c2FkbWlucyBMVjEcMBoGA1UECxMTSW5mb3JtYXRpb24g
# U3lzdGVtczEWMBQGA1UEAxMNVmFkaW1zIFBvZGFuczCCASIwDQYJKoZIhvcNAQEB
# BQADggEPADCCAQoCggEBAIcw8V5Bjn11ZLAG/GhiQ+y7CEpYt/Z6alFQdkBNPSHu
# WMC+ebPUQgEky57JOeo9DeXUv8+rOxOt1thptEDEIZ5tJQHhSxLEfoxLSHQCkn4O
# mQXk6q/UZWfvktv73k2Rq+xdtvmMFTH4xqvhddVma6MeKEBWPu5URhT7wvnI+cGh
# 5TeE8kmErq/E2hVIOeZ1r85IC1naBiV4VxJMMQkePswBTYCAcjYCT1UU8GihEdgq
# 8dClNmsE2a/dYNoTktxIGUk2wFnP/ptSEtrlzhczKa5WDlGeuMx62lfRuTfzq+gO
# zk4JDleud6NPqqIijh/iYBS+qJ+4GexYPL0wZCdTPVUCAwEAAaOCAYEwggF9MDsG
# CSsGAQQBgjcVBwQuMCwGJCsGAQQBgjcVCJadTYWSsni9nzyF6Ox0gs7YRHqCqvdC
# h+fENgIBZAIBAzAfBgNVHSUEGDAWBgorBgEEAYI3CgMMBggrBgEFBQcDAzAOBgNV
# HQ8BAf8EBAMCB4AwKQYJKwYBBAGCNxUKBBwwGjAMBgorBgEEAYI3CgMMMAoGCCsG
# AQUFBwMDMB0GA1UdDgQWBBQsddpa07a5NYAClLLmLzGiK9dXmTAfBgNVHSMEGDAW
# gBQb+l5zLWcTXM7TDuboeqlgjAtj/DA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8v
# d3d3LnN5c2FkbWlucy5sdi9wa2kvcGljYS0xLmNybDBpBggrBgEFBQcBAQRdMFsw
# MgYIKwYBBQUHMAKGJmh0dHA6Ly93d3cuc3lzYWRtaW5zLmx2L3BraS9waWNhLTEu
# Y3J0MCUGCCsGAQUFBzABhhlodHRwOi8vb2NzcC5zeXNhZG1pbnMubHYvMA0GCSqG
# SIb3DQEBBQUAA4IBAQBJ2bGbZu+3T+0ZJXOTSjQfXfAcBzzqHM+R16Up6qXkjUnQ
# gguINT/1Ktqr3y7SdPGkyHZytqz0ABwr/hgZ1bdl4WaV9xpy4oJni7wU4Gq6Mh8Q
# zvhGwrQmQifbRyumM/EKMzyYZU+KkD7TAHoN1CiEGhiEyK+9OVaQNxAxwO3jmWWN
# cj2Q86YrV7r+XzkAU/N6gSeVUXii5eGA30wQNnCWQd2cTzL9tHdNH8t4qKN9Lhij
# t0EoxGEZYGDniROmIYlIwZUj6nU/XsmeHyJ5vpcvBxu12AVQMNIUY+HzCLStKnCy
# Sd1htmJBemlaam0OPeYp7QSUKgwzm1+gK813GUzKMYIDpTCCA6ECAQEwgYAwcjEL
# MAkGA1UEBhMCTFYxFTATBgNVBAoTDFN5c2FkbWlucyBMVjEcMBoGA1UECxMTSW5m
# b3JtYXRpb24gU3lzdGVtczEuMCwGA1UEAxMlU3lzYWRtaW5zIExWIEludGVybmFs
# IENsYXNzIDEgU3ViQ0EtMQIKYTydVQAAAAAAEzAJBgUrDgMCGgUAoHgwGAYKKwYB
# BAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAc
# BgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUeTun
# 2BuUHwBtmzK31bA8+JUR9z8wDQYJKoZIhvcNAQEBBQAEggEAAkgMmQZ3dPbeckES
# 4IzwcQlRkntdrbW9YeUvukl0hIBg3lAhNKYizsB37yQGscTbhjiOGOuIiojf/nSU
# EtMkpMDq2Msb9V5ex/cTodtq5/hCrRhnk7g0ew0W5rw6cqdSq9/5tqvH3w5ZvdWA
# NRawXF53FH/5Mg/XFYJMRkk9D0yQYwNjIJe0lWNKYf4lnpfMx5P1mFuFnQkteUS/
# dI/zwaHFT+UsdLxru5dAWJHj2//039csH2zZx6o23wGzKiAMkQWn5F8I5GfhyoHx
# w5nRLgXZ/w+chX5WnCYCtIaRlCthbPIrGkfy5Qs9l/Bq6Ay5dBn24rgcZUXSavDA
# lEbKGKGCAX8wggF7BgkqhkiG9w0BCQYxggFsMIIBaAIBATBnMFMxCzAJBgNVBAYT
# AlVTMRcwFQYDVQQKEw5WZXJpU2lnbiwgSW5jLjErMCkGA1UEAxMiVmVyaVNpZ24g
# VGltZSBTdGFtcGluZyBTZXJ2aWNlcyBDQQIQOCXX+vhhr570kOcmtdZa1TAJBgUr
# DgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUx
# DxcNMTEwODExMTEyMTMwWjAjBgkqhkiG9w0BCQQxFgQU5Gv3jWdoNRExPdBN8184
# 3U3wzDgwDQYJKoZIhvcNAQEBBQAEgYC/rw1QYuDbNBhEpT6CgY8odiWkzr9Cr0S8
# iIoFg+oad8WAm58JoafOxhzjaEUFKO9Tzks58F5h0oZWWybufJRRTbp/gqd4B7dw
# cQoX+RzfTInWXRlnqvsyclEJ8+7wfM/c//38ObQoHtoslyN7wBRHN6PgQteUkp/l
# i9doeEQUCQ==
# SIG # End signature block
