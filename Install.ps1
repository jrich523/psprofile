﻿#check for PS user folder, this is where user profiles should live
$profilePath = split-path $profile
if(-not (Test-Path $profilePath))
{
    $null=mkdir $profilePath
}

##############
#inject profiles - this will only work for user profiles
$profilelist = gci $PSScriptRoot "*profile.ps1"
foreach($p in $profilelist)
{
    $pPath = Join-Path $profilePath $p.Name
    $script = ". $($p.FullName)" 
    if(Test-Path $pPath)
    {
        #add it to the current profile
        if(! (Select-String -Path $pPath -Pattern $script -SimpleMatch -Quiet))
        {
            $script | Out-File $pPath -Append
        }
    }
    else
    {
        #create profile and inject
        $script | Out-File $pPath -Force
        
    }
}
##############
#add ISE snippets
$snippets = join-path $PSScriptRoot 'Snippets'

if(gci $snippets -ErrorAction SilentlyContinue)
{
    Copy-Item $snippets $profilePath -Force -Recurse
}
##############
#add module path to env var
$currentValue = [environment]::GetEnvironmentVariable("PSModulePath","User")
[Environment]::SetEnvironmentVariable("PSModulePath", "$CurrentValue;$(join-path $PSScriptRoot 'Modules')" , "User")

###############
#add themes (theme folder)
$themeExtension = '.StorableColorTheme.ps1xml'
$themeFiles = gci (join-path $PSScriptRoot 'Themes') "*$themeExtension" -ErrorAction SilentlyContinue
$themeRootPath = "HKCU:\Software\Microsoft\PowerShell\3\Hosts\PowerShellISE\ColorThemes"
if(-not (test-path $themeRootPath))
{
     new-item $themeRootPath -Force
}
$themeRoot = gi HKCU:\Software\Microsoft\PowerShell\3\Hosts\PowerShellISE\ColorThemes
$themeNames = $themeRoot.GetValueNames()
foreach($file in $themeFiles)
{
    $name =$file.Name.Replace($themeExtension,"") 
    if($name -in $themeNames)
    {
        #it already exists, maybe check to see if its the same details
    }
    else
    {
        #doesnt exists, add it
        #$themeRoot.SetValue($name, (gc $file.FullName))
        $null = New-ItemProperty -Path $themeRootPath -Name $name -Value ((gc $file.FullName) -join "")
    }
}


